package com.marion.zenikook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZenikookApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZenikookApplication.class, args);
	}

}
